'use strict'

const express = require('express')
const app = express()
const cors = require('cors')
const jwt = require('./helpers/jwt')
const errorHandler = require('./helpers/error-handler')
const http = require('http')
const auth = require('./routes/auth/credentials')
const user = require('./routes/c/users')
const animal = require('./routes/c/animal')
const event = require('./routes/c/events')
const adopt = require('./routes/c/adopt')
var admin = require("firebase-admin")

var serviceAccount = require("../serviceAccountKey.json")

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://amor-de-pet-d7170.firebaseio.com"
});

const debug = require('debug')('adp:server')

const port = normalizePort(process.env.PORT || '3000')
app.set('port', port)

app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));

app.use(cors({ origin: '*' }))
app.use(jwt())

const logIncomingRequest = (req, res, next) => {
    debug(`HEADER ${JSON.stringify(req.headers, null, 2)}`)

    if(req.method !== 'GET' && req.get("content-length") && parseInt(req.get("content-length")) < 200000) {
        debug(`BODY ${JSON.stringify(req.body, null, 2)}`)
    }

    next()
}

function normalizePort(val) {
    let port = parseInt(val, 10)
  
    if (isNaN(port)) {
      return val
    }
  
    if (port >= 0) {
      return port
    }
  
    return false
}

//subir a porta 3000, em caso de erro, mostrar qual
const server = http.createServer(app)
server.listen(port)
server.on('listening', () => {
    let addr = server.address()
    let bind = typeof addr === 'string'
      ? 'pipe ' + addr
      : 'port ' + addr.port
    debug('Listening on ' + bind)
})
server.on('error', error => {
    if (error.syscall !== 'listen') {
      throw error
    }
  
    let bind = typeof port === 'string'
      ? 'Pipe ' + port
      : 'Port ' + port
  
    switch (error.code) {
      case 'EACCES':
        console.error(bind + ' requires elevated privileges')
        process.exit(1)
        break
      case 'EADDRINUSE':
        console.error(bind + ' is already in use')
        process.exit(1)
        break
      default:
        throw error
    }
})

// api routes
app.use('/api/signin', [logIncomingRequest], auth)
app.use('/api/user', [logIncomingRequest], user)
app.use('/api/animal', [logIncomingRequest], animal)
app.use('/api/event', [logIncomingRequest], event)
app.use('/api/adopt', [logIncomingRequest], adopt)

// global error handler
app.use(errorHandler)

module.exports = app
