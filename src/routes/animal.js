'use strict'

const db = require('../helpers/db')
const admin = require('firebase-admin')
const stream = require('stream')
const crypto = require('crypto')
const debug = require('debug')('adp:server')
const Animal = db.Animal
const Adopt = db.Adopt

async function getAll() {
	return await Animal.find()
}

async function getById(id) {
	let animal = await Animal.findById(id)

	//animal = animal.lean.exec()
	animal = animal.toObject()

	animal.gallery = await getImagesGallery(animal)
	animal.anexo = await getImagesAnexo(animal)
	animal.term = await getFileTerm(animal)

	return animal;
}

async function getFileTerm(animal) {
	let adopt = await Adopt.findOne({ animal: animal._id })

	if (adopt) {
		let id = adopt._id

		const bucket = admin.storage().bucket('amor-de-pet-d7170.appspot.com')

		const files = await bucket.getFiles({
			prefix: `termo/${id}/`
		})

		if (!files[0]) return

		const urls = files[0].map(async file => {
			const parts = file.name.split('/')
			const filename = parts[parts.length - 1]

			const url = await file.getSignedUrl({
				action: 'read',
				expires: '03-17-2025'
			})

			return {
				file: filename,
				url: url
			}
		})

		return await Promise.all(urls)
	}

}

async function getImagesGallery(animal) {
	let id = animal._id

	const bucket = admin.storage().bucket('amor-de-pet-d7170.appspot.com')

	const files = await bucket.getFiles({
		prefix: `galeria/${id}/`
	})

	if (!files[0]) return []

	const urls = files[0].map(async file => {
		const parts = file.name.split('/')
		const filename = parts[parts.length - 1]

		debug(`Buscando imagem ${file.name}`)

		const url = await file.getSignedUrl({
			action: 'read',
			expires: '03-17-2025'
		})

		return {
			file: filename,
			url: url
		}
	})

	return await Promise.all(urls)
}

async function getImagesAnexo(animal) {
	let id = animal._id

	const bucket = admin.storage().bucket('amor-de-pet-d7170.appspot.com')

	const files = await bucket.getFiles({
		prefix: `anexo/${id}/`
	})

	if (!files[0]) return []

	const urls = files[0].map(async file => {
		const parts = file.name.split('/')
		const filename = parts[parts.length - 1]

		debug(`Buscando imagem ${file.name}`)

		const url = await file.getSignedUrl({
			action: 'read',
			expires: '03-17-2025'
		})

		return {
			file: filename,
			url: url
		}
	})

	return await Promise.all(urls)
}

async function register(param) {
	const {
		urlProfile,
		name,
		age,
		castrated,
		type,
		race,
		gender,
		health,
		dateRegistry,
		adopt,
		dateAdopt,
		obs
	} = param

	const data = {
		name,
		age,
		castrated,
		type,
		race,
		gender,
		health,
		dateRegistry,
		adopt,
		dateAdopt,
		obs
	}

	const animal = new Animal(data)

	await animal.save()

	const dataAnimal = await Animal.findOne(animal)
	const id = dataAnimal._id
	createFileProfile(id, urlProfile).catch(console.error)
}

async function createFileProfile(id, url) {
	const bucket = admin.storage().bucket('amor-de-pet-d7170.appspot.com')
	const filename = crypto.randomBytes(10).toString('hex')

	const animal = await Animal.findById(id)

	if (url.substring(0, 4) === 'data') {

		const promise = new Promise((resolve, reject) => {
			let fileB64 = url.split(';')
			let mime = fileB64[0].split(':')[1]

			if (!fileB64) resolve()

			let decodedImage = new Buffer.from(fileB64[1].substring(7), 'base64')
			let bufferStream = new stream.PassThrough()
			bufferStream.end(decodedImage)

			let file = bucket.file(`profile/${id}/${filename}`)

			bufferStream
				.pipe(file.createWriteStream({
					metadata: {
						contentType: mime
					}
				}))
				.on('error', err => {
					reject(err)
				})
				.on('finish', () => {
					resolve()
				})			
		})

		await Promise.resolve(promise)

		let file = bucket.file(`profile/${id}/${filename}`)

		const urlProfile = await file.getSignedUrl({
			action: 'read',
			expires: '03-17-2025'
		})
	
		const f = { file: filename, url: urlProfile }
		animal.urlProfile = f

		await animal.save()

	} else {
		const file = { file: filename, url: url }
		animal.urlProfile = file

		await animal.save()
	}
}

async function update(id, body) {
	const animal = await Animal.findById(id)

	Object.assign(animal, body)

	await animal.save()
}

async function updateFileProfile(id, body) {
	const bucket = admin.storage().bucket('amor-de-pet-d7170.appspot.com')

	const url = body.url
	const filename = body.file

	const animal = await Animal.findById(id)

	// Se for base64 e não tiver filename
	// é porque o usuário está fazendo a troca de avatar para uma imagem do pc
	// então precisa deixar o urlProfile null para que ele não puxe o mesmo avatar
	// e sim a foto que está fazendo upload
	if (url.substring(0, 4) === 'data' && filename === undefined) {
		animal.urlProfile = null

		await animal.save()

		createFileProfile(id, url).catch(console.error)
	}
	// se tiver o filename, é porque está trocando uma foto já cadastrada do pc por outra
	else if (url.substring(0, 4) === 'data') {
		await bucket.file(`profile/${id}/${filename}`).delete()
		debug(`File ${filename} deleted.`)

		createFileProfile(id, url).catch(console.error)
	}
	// se for url, ele salva o link na urProfile do animal no banco de dados
	else {
		const name = crypto.randomBytes(10).toString('hex')
		const file = { file: name, url: url }
		animal.urlProfile = file

		animal.save()
	}
}

async function uploadFileGallery(id, body) {
	const bucket = admin.storage().bucket('amor-de-pet-d7170.appspot.com')

	if (!body.url) return

	let promises = []

	let element = body.url
	let fileName = body.file

	promises.push(new Promise((resolve, reject) => {
		let fileB64 = element.split(';')
		let mime = fileB64[0].split(':')[1]

		if (!fileB64[1]) resolve()

		let decodedImage = new Buffer.from(fileB64[1].substring(7), 'base64')
		let bufferStream = new stream.PassThrough()
		bufferStream.end(decodedImage)

		let file = bucket.file(`galeria/${id}/${fileName}`)

		bufferStream
			.pipe(file.createWriteStream({
				metadata: {
					contentType: mime
				}
			}))
			.on('error', err => {
				reject(err)
			})
			.on('finish', () => {
				resolve()
			})
	}))


	await Promise.all(promises)
}

async function uploadFileAnexo(id, body) {
	const bucket = admin.storage().bucket('amor-de-pet-d7170.appspot.com')

	if (!body.url) return

	let promises = []

	let element = body.url
	let fileName = body.file

	promises.push(new Promise((resolve, reject) => {
		let fileB64 = element.split(';')
		let mime = fileB64[0].split(':')[1]

		if (!fileB64[1]) resolve()

		let decodedImage = new Buffer.from(fileB64[1].substring(7), 'base64')
		let bufferStream = new stream.PassThrough()
		bufferStream.end(decodedImage)

		let file = bucket.file(`anexo/${id}/${fileName}`)

		bufferStream
			.pipe(file.createWriteStream({
				metadata: {
					contentType: mime
				}
			}))
			.on('error', err => {
				reject(err)
			})
			.on('finish', () => {
				resolve()
			})
	}))


	await Promise.all(promises)
}

async function deleteImage(id, name, pack) {

	const bucket = admin.storage().bucket('amor-de-pet-d7170.appspot.com')
	await bucket.file(`${pack}/${id}/${name}`).delete()
	debug(`File ${name} deleted.`)
}

async function _delete(id) {
	await Animal.findByIdAndRemove(id)
}

module.exports = {
	getAll,
	getById,
	register,
	uploadFileGallery,
	update,
	updateFileProfile,
	uploadFileAnexo,
	deleteImage,
	delete: _delete
}