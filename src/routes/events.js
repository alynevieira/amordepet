'use strict'

const db = require('../helpers/db')
const Event = db.Event

async function getAllEvents() {
    return await Event.find()
}

async function getEventById(id) {
    let event = await Event.findById(id)

    return event
}

async function registerEvent(param) {
    const event = new Event(param)

    await event.save()
}

async function updateEvent(id, body) {
    const event = await Event.findById(id)

    Object.assign(event, body)
    
    await event.save()
}

async function deleteEvent(id) {
    await Event.findByIdAndRemove(id)
}

module.exports = {
    getAllEvents,
    getEventById,
    registerEvent,
    updateEvent,
    deleteEvent
}