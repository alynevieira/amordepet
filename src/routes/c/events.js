'use strict'

const express = require('express')
const router = express.Router()

const event = require('../events')

router.get('/', getAllEvents)
router.get('/:id', getEventById)
router.post('/register', registerEvent)
router.put('/:id', updateEvent)
router.delete('/:id', deleteEvent)

function getAllEvents(req, res, next) {
    event.getAllEvents()
    .then(event => res.json(event))
    .catch(err => next(err))
}

function getEventById(req, res, next) {
    event.getEventById(req.params.id)
    .then(event => event ? res.json(event) : res.sendStatus(404))
    .catch(err => next(err))
}

function registerEvent(req, res, next) {
    event.registerEvent(req.body)
        .then(() => res.json({}))
        .catch(err => next(err))
}

function updateEvent(req, res, next) {
    event.updateEvent(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err))
}

function deleteEvent(req, res, next) {
    event.deleteEvent(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}

module.exports = router