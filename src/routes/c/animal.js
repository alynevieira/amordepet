'use strict'

const express = require('express')
const router = express.Router()
const animal = require('../animal')

router.get('/', getAll)
router.get('/:id', getById)
router.post('/register', register)
router.post('/:id/uploadFileGallery', uploadFileGallery)
router.post('/:id/uploadFileAnexo', uploadFileAnexo)
router.put('/:id', update)
router.put('/:id/updateFileProfile', updateFileProfile)
router.delete('/:id', _delete)
router.delete('/:id/deleteImage/files/:filename/:pack', deleteImage)

function getAll(req, res, next) {
    animal.getAll()
    .then(animal => res.json(animal))
    .catch(err => next(err))
}

function getById(req, res, next) {
    animal.getById(req.params.id)
    .then(animal => animal ? res.json(animal) : res.sendStatus(404))
    .catch(err => next(err))
}

function register(req, res, next) {
    animal.register(req.body)
        .then(() => res.json({}))
        .catch(err => next(err))
}

function uploadFileGallery(req, res, next) {
    animal.uploadFileGallery(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err))
}

function uploadFileAnexo(req, res, next) {
    animal.uploadFileAnexo(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err))
}

function updateFileProfile(req, res, next) {
    animal.updateFileProfile(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err))
}

function update(req, res, next) {
    animal.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err))
}

function deleteImage(req, res, next) {
    animal.deleteImage(req.params.id, req.params.filename, req.params.pack)
        .then(() => res.json({}))
        .catch(err => next(err))
}

function _delete(req, res, next) {
    animal.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err))
}

module.exports = router