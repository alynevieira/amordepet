'use strict'

const express = require('express')
const router = express.Router()
const adopt = require('../adopt')

const db = require('../../helpers/db')
const Adopt = db.Adopt

router.get('/', getAllAdopt)
router.get('/:id', getAdoptById)
router.get('/user/:id', getAdoptByUser)
router.get('/animal/:id', getAdoptByAnimal)
router.post('/registerAdopt', registerAdopt)
router.post('/:id/uploadFileTerm', uploadFileTerm)
router.post('/:id/uploadFileForm', uploadFileForm)
router.put('/:id', update)
router.delete('/:id', _delete)

async function getAllAdopt(req, res, next) {
  await Adopt.find()
  .populate('animal')
  .populate('adopter').select('-password')
  .exec(function (err, adopt) {
    if (err) {
      next(err)
    }

    res.json(adopt)
  })
}

function getAdoptById(req, res, next) {
  adopt.getAdoptById(req.params.id)
  .then(adopt => adopt ? res.json(adopt) : res.sendStatus(404))
  .catch(err => next(err))
}

async function getAdoptByUser(req, res, next) {
  await Adopt.find({ adopter: req.params.id })
  .populate('animal')
  .exec(function (err, animal) {
    if (err){
      next(err)
    }
          
    res.json(animal)
  })
}

async function getAdoptByAnimal(req, res, next) {
  await Adopt.findOne({ animal: req.params.id })
  .populate('adopter').select('-password')
  .exec(async function (err, adopt) {
    if (err){
      next(err)
    }
          
    res.json(adopt)
  })
}

function registerAdopt(req, res, next) {
  adopt.create(req.body)
  .then(adopt => adopt ? res.json(adopt) : res.sendStatus(404))
  .catch(err => next(err))
}

function update(req, res, next) {
  adopt.update(req.params.id, req.body)
  .then(() => res.json({}))
  .catch(err => next(err))
}

function uploadFileTerm(req, res, next) {
  adopt.uploadFileTerm(req.params.id, req.body)
    .then(() => res.json({}))
    .catch(err => next(err))
}

function uploadFileForm(req, res, next) {
  adopt.uploadFileForm(req.params.id, req.body)
    .then(() => res.json({}))
    .catch(err => next(err))
}

function _delete(req, res, next) {
  adopt._delete(req.params.id)
  .then(() => res.json({}))
  .catch(err => next(err))
}

module.exports = router