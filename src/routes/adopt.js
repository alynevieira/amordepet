'use strict'

const db = require('../helpers/db')
const debug = require('debug')('adp:server')
const Adopt = db.Adopt
const Animal = db.Animal

async function getAll() {
  return await Adopt.find()
}

async function getAdoptById(id) {
  await Adopt.findOne({ _id: id })
}

async function create(param) {    
  const adopt = new Adopt({
    process: param.process,
    adopter: param._idUser,
    animal: param._idAnimal
  })

  const animal = await Animal.findById(param._idAnimal)

  animal.adopt = "Adotado"
  animal.dateAdopt = new Date().toISOString().replace(/T.+/, '')

  await animal.save()
  await adopt.save()

  return adopt
}

async function update(id, body) {
  const adopt = await Adopt.findById(id)

  Object.assign(adopt, body)
  
  await adopt.save()
}

async function _delete(id) {
  const adopt = await Adopt.findById(id)
  const animal = await Animal.findById(adopt.animal)

  animal.adopt = "Não adotado"
  animal.dateAdopt = ""

  await animal.save()

  await Adopt.findByIdAndRemove(id)
}

module.exports = {
  getAll,
  getAdoptById,
  update,
  _delete,
  create
}