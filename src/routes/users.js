'use strict'

const db = require('../helpers/db')
const User = db.User
const admin = require('firebase-admin')
const { v4: uuidv4 } = require('uuid')
const stream = require('stream')

async function getAll() {
	return await User.find().select('-password')
}

async function getById(id) {
	return await User.findById(id).select('-password')
}

async function uploadSignedTerm(filename, url, id) {
	const bucket = admin.storage().bucket('amor-de-pet-d7170.appspot.com')

	if (!url) return
	
	if (!filename) filename = uuidv4()

	let element = url

	new Promise(async (resolve, reject) => {
		let fileB64 = element.split(';')
		let mime = fileB64[0].split(':')[1]

		if (!fileB64[1]) resolve()

		let decodedImage = new Buffer.from(fileB64[1].substring(7), 'base64')
		let bufferStream = new stream.PassThrough()
		bufferStream.end(decodedImage)

		let file = bucket.file(`termoAssinado/${id}/${filename}`)

		bufferStream
			.pipe(file.createWriteStream({
				metadata: {
					contentType: mime
				}
			}))
			.on('error', err => {
				reject(err)
			})
			.on('finish', () => {
				resolve()
			})

			const urlTerm = await file.getSignedUrl({
				action: 'read',
				expires: '03-17-2025'
			})

			let user = User.findById(id)
		
			user.signedTerm = {
        file: filename,
        url: urlTerm
    	}
		
			await user.save()
	})

}

async function update(id, userParam) {
	const user = await User.findById(id)

	// validate
	if (!user) throw 'Usuário não encontrado.';
	if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
		throw 'Usuário "' + userParam.username + '" já está sendo usado.'
	}

	if (user.email !== userParam.email && await User.findOne({ email: userParam.email })) {
		throw 'Email "' + userParam.email + '" já está cadastrado.'
	}

	Object.assign(user, userParam)

	await user.save()
}

async function _delete(id) {
	await User.findByIdAndRemove(id)
}

module.exports = {
	getAll,
	getById,
	uploadSignedTerm,
	update,
	delete: _delete
}