'use strict'

const express = require('express')
const router = express.Router()
const config = require('../../config.json')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const db = require('../../helpers/db')
const nodemailer = require('nodemailer')
const debug = require('debug')('adp:server')
const admin = require('firebase-admin')
const { v4: uuidv4 } = require('uuid')
const stream = require('stream')

const User = db.User

router.post('/authenticate', async (req, res) => {

	const username = req.body.username
	const password = req.body.password

	const user = await User.findOne({ username })

	if (user && bcrypt.compareSync(password, user.password)) {
		const { password, ...userWithoutHash } = user.toObject()
		const token = jwt.sign({ sub: user.id }, config.secret)
		const role = user.role
		res.json({
			...userWithoutHash,
			token,
			role
		})
	} else {
		res.status(400).json({ message: 'Usuário ou senha incorreto.' })
	}
})

router.post('/register', async (req, res) => {
	const {
		firstname,
		lastname,
		phone,
		cellphone,
		email,
		address: {
			cep,
			street,
			numberhouse,
			neighborhood,
			city,
			state
		},
		username,
		password,
		role,
		term
	} = req.body

	const data = {
		firstname,
		lastname,
		phone,
		cellphone,
		email,
		address: {
			cep,
			street,
			numberhouse,
			neighborhood,
			city,
			state
		},
		username,
		password,
		role
	}

	if (await User.findOne({ username: username })) {
		res.status(400).json({ message: 'Usuário "' + username + '" já existe.' })

		return
	}

	if (await User.findOne({ email: email })) {
		res.status(400).json({ message: 'Email "' + email + '" já cadastrado.' })

		return
	}

	const user = new User(data)

	if (password) {
		user.password = bcrypt.hashSync(password, 10);
	}

	if (!user.role) {
		user.role = 'User'
	}

	await user.save()

	const dataUser = await User.findOne(user)

	uploadFileTerm(dataUser, term)

	if (!username && !password) {
		var from = 'noreply.mundopet@gmail.com'
		var to = email
		let transporter = nodemailer.createTransport({
			host: 'smtp.googlemail.com',
			port: 465,
			secure: true,
			auth: {
				user: 'noreply.mundopet@gmail.com',
				pass: 'MeAbMeBe2114*'
			}
		})

		let mailOptions = {
			from: from,
			to: to,
			subject: 'Criar um usuário - Mundo pet',
			text: 'Olá, aqui está o link para a criação do usuário: https://amordepet.netlify.app/username/' + dataUser._id
		}

		transporter.sendMail(mailOptions, (error, info) => {
			if (error) {
				return debug(error)
			}
			debug('Message sent: %s', info.messageId)
		})
	} else {
		debug('Email not found')
	}

	delete user.password

	res.json(user)
})

router.put('/username', async (req, res) => {
	const {
		_id,
		username,
		password
	} = req.body

	const data = {
		username,
		password
	}

	if (await User.findOne({ username: username })) {
		res.status(400).json({ message: 'Usuário "' + username + '" já existe.' })

		return
	}

	const user = await User.findById(_id)

	if (password) {
		user.password = bcrypt.hashSync(password, 10);
	}

	Object.assign(user, data)

	await user.save()
})

router.post('/forgetPassword', async (req, res) => {
	const email = req.body.email

	const user = await User.findOne({ email })

	if (user) {
		var from = 'noreply.mundopet@gmail.com'
		var to = email
		let transporter = nodemailer.createTransport({
			host: 'smtp.googlemail.com',
			port: 465,
			secure: true,
			auth: {
				user: 'noreply.mundopet@gmail.com',
				pass: 'MeAbMeBe2114*'
			}
		})

		let mailOptions = {
			from: from,
			to: to,
			subject: 'Cadastrar nova senha ',
			text: 'Link para o acesso a troca de senha: https://amordepet.netlify.app/password/' + user._id
		}

		transporter.sendMail(mailOptions, (error, info) => {
			if (error) {
				return debug(error)
			}
			debug('Message sent: %s', info.messageId)
		})
	} else {
		debug('Email not found')
	}

	res.json(user)
})

router.put('/newPassword', async (req, res) => {
	const userParam = req.body
	const user = await User.findOne({ _id: userParam._id })
	const pass = await User.findById(user).select('password')

	// validate
	if (await !user) throw 'Usuário não encontrado.';

	if (userParam.oldPassword) {
		const valid = await bcrypt.compare(userParam.oldPassword, pass.password)
		if (!valid) {
			debug(pass.password)
			return res.status(400).json({ message: 'Senha atual incorreta.' })
		}
		if (userParam.oldPassword === userParam.password) {
			return res.status(400).json({ message: 'A nova senha não pode ser a mesma da antiga senha.' })
		}
	}

	if (userParam.password) {
		userParam.password = bcrypt.hashSync(userParam.password, 10)
	}

	Object.assign(user, userParam)

	await user.save()

	res.json(user)
})

async function uploadFileTerm(user, url) {
	const bucket = admin.storage().bucket('amor-de-pet-d7170.appspot.com')

	if (!url) return

	let element = url
	let filename = uuidv4() + '.pdf'

	new Promise(async (resolve, reject) => {
		let fileB64 = element.split(';')
		let mime = fileB64[0].split(':')[1]

		if (!fileB64[2]) resolve()

		let decodedImage = new Buffer.from(fileB64[2].substring(7), 'base64')
		let bufferStream = new stream.PassThrough()
		bufferStream.end(decodedImage)

		let file = bucket.file(`termo/${user._id}/${filename}`)

		bufferStream
			.pipe(file.createWriteStream({
				metadata: {
					contentType: mime
				}
			}))
			.on('error', err => {
				reject(err)
			})
			.on('finish', () => {
				resolve()
			})

		const urlTerm = await file.getSignedUrl({
			action: 'read',
			expires: '03-17-2025'
		})
	
		user.term = urlTerm
	
		await user.save()
	})
	
}

module.exports = router