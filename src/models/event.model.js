'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new Schema({
    name: String,
    dateEvent: String,
    hourEvent: String,
    address: String,
    description: String,
    latitude: Number,
    longitude: Number
});

schema.set('toJSON', { virtuals: true })

module.exports = mongoose.model('Event', schema, 'event')