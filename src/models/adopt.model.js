'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new Schema({
    process: String,
    adopter: {
      type: Schema.Types.ObjectId,
      ref: "User"
    },
    animal: {
      type: Schema.Types.ObjectId,
      ref: "Animal"
    }
});

schema.set('toJSON', { virtuals: true })

module.exports = mongoose.model('Adopt', schema, 'adopt')