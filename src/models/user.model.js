'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new Schema({
    firstname: String,
    lastname: String,
    phone: String,
    cellphone: String,
    email: String,
    address: {
        cep: String,
        street: String,
        numberhouse: String,
        neighborhood: String,
        city: String,
        state: String
    },
    username: String,
    password: String,
    hash: String,
    role: String,
    term: Array,
    signedTerm: {
        file: String,
        url: Array
    },
    answer: {
        answer1: String,
        answer2: String,
        answer3: String,
        answer4: String,
        answer5: String,
        answer6: String,
    },
    termAgree: Boolean,
    createdDate: { type: Date, default: Date.now },
    adoptions: [{
        type: Schema.Types.ObjectId,
        ref: "Adopt"
    }]
});

schema.set('toJSON', { virtuals: true })

module.exports = mongoose.model('User', schema, 'users')