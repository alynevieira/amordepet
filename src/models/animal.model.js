'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const schema = new Schema({
  urlProfile:{
    file: String,
    url: Array
  },
  name: String,
  age: String,
  castrated: String,
  type: String,
  race: String,
  gender: String,
  health: String,
  dateRegistry: String,
  adopt: String,
  dateAdopt: String,
  obs: String,
  adoption: {
    type: Schema.Types.ObjectId,
    ref: "Adopt"
  }
});

schema.set('toJSON', { virtuals: true })

module.exports = mongoose.model('Animal', schema, 'animal')