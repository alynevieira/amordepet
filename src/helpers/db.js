'use strict'

;(async() => {
    const config = require('../config.json')
    const mongoose = require('mongoose')
    const debug = require('debug')('adp:server')

    await mongoose.connect(process.env.MONGODB_URI || config.connectionString, { useCreateIndex: true, useNewUrlParser: true })
    mongoose.Promise = global.Promise

    debug('MongoDB connected')
})()

module.exports = {
    User: require('../models/user.model'),
    Animal: require('../models/animal.model'),
    Event: require('../models/event.model'),
    Adopt: require('../models/adopt.model'),
}